package mapper.proyecto;

import java.sql.ResultSet;

import vo.proyecto.ProductoVO;

public class ProductMapper {
	
	private String tipoMapper;
	
	public ProductMapper(String tipoMapper) {
        super();
        this.setTipoMapper(tipoMapper);
    }
	
	public String getTipoMapper() {
        return tipoMapper;
    }

    public void setTipoMapper(String tipoMapper) {
        this.tipoMapper = tipoMapper;
    }
    
    public ProductoVO mapRow(ResultSet paramResultSet) {
    	ProductoVO producto = new ProductoVO();
    	
    	try {
    		if("1".equals(tipoMapper)) {
    			producto.setAltoProduct(paramResultSet.getString("ALTO"));
    			producto.setAnchoProduct(paramResultSet.getString("ANCHO"));
    			producto.setColorProduct(paramResultSet.getString("COLOR"));
    			producto.setCostoProduct(paramResultSet.getInt("COSTO_UNITARIO"));
    			producto.setDescripcionProduct(paramResultSet.getString("DESCRIPCION"));
    			producto.setDestacadoProduct(paramResultSet.getBoolean("FLAG_DESTACADO"));
    			producto.setIdImagenProduct(paramResultSet.getString("ID_IMAGEN"));
    			producto.setIdProduct(paramResultSet.getString("ID_PRODUCTO"));
    			producto.setRetenidoProduct(paramResultSet.getInt("RETENIDO"));
    			producto.setStockProduct(paramResultSet.getInt("STOCK"));
    			producto.setTipoProduct(paramResultSet.getString("TIPO_PRODUCTO"));
    			producto.setNombreProducto(paramResultSet.getString("NOMBRE"));
    		}
    	}catch(Exception e) {
    		
    	}
    	
    	return producto;
    }

}
