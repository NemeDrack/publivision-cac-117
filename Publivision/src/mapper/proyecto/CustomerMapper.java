package mapper.proyecto;

import java.sql.ResultSet;

import vo.proyecto.ClienteVO;
import vo.proyecto.DireccionVO;

public class CustomerMapper {
	
private String tipoMapper;
	
	public CustomerMapper(String tipoMapper) {
        super();
        this.setTipoMapper(tipoMapper);
    }
	
	public String getTipoMapper() {
        return tipoMapper;
    }

    public void setTipoMapper(String tipoMapper) {
        this.tipoMapper = tipoMapper;
    }
    
    public ClienteVO mapRow(ResultSet paramResultSet) {
    	ClienteVO cliente = new ClienteVO();
    	
    	try {
    		if("1".equals(tipoMapper)) {
    			DireccionVO direccion = new DireccionVO();
    			direccion.setCiudad(paramResultSet.getString("CIUDAD"));
    			direccion.setComentarioRef(paramResultSet.getString("COMENTARIO_REFERENCIA"));
    			direccion.setIdDireccion(paramResultSet.getInt("ID_DIRECCION"));
    			direccion.setNombreCalle(paramResultSet.getString("NOMBRE_CALLE"));
    			direccion.setNombreComplejo(paramResultSet.getString("NOMBRE_COMPLEJO"));
    			direccion.setNumeroCalle(paramResultSet.getInt("NUMERO_CALLE"));
    			direccion.setPais(paramResultSet.getString("PAIS"));
    			direccion.setPiso(paramResultSet.getInt("PISO"));
    			direccion.setRegion(paramResultSet.getString("REGION"));
    			direccion.setTipoDireccion(paramResultSet.getString("TIPO_DIRECCION"));
    			cliente.setDireccion(direccion);
    			cliente.setCorreo(paramResultSet.getString("CORREO"));
    			cliente.setFlagCorporate(paramResultSet.getInt("FLAG_CORPORATE"));
    			cliente.setFlagDestacado(paramResultSet.getInt("FLAG_DESTACADO"));
    			cliente.setGiro(paramResultSet.getString("GIRO"));
    			cliente.setIdDireccion(paramResultSet.getInt("ID_DIRECCION"));
    			cliente.setNombreRazon(paramResultSet.getString("NOMBRE_RAZON"));
    			cliente.setRutCliente(paramResultSet.getInt("ID_CLIENTE"));
    			cliente.setApellidos(paramResultSet.getString("APELLIDOS"));
    			cliente.setCorreo(paramResultSet.getString("CORREO"));
    		}
    	}catch(Exception e) {
    		String error = e.getMessage();
    		cliente = null;
    	}
    	
    	return cliente;
    }

}
