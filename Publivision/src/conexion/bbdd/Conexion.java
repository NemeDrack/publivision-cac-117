/**
 * 
 */
package conexion.bbdd;

/**
 * @author rzunigas
 *
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import mapper.proyecto.CustomerMapper;
import mapper.proyecto.ProductMapper;
import vo.proyecto.ClienteVO;
import vo.proyecto.CotizacionVO;
import vo.proyecto.ProductoVO;

public class Conexion {
	Connection conn;
	Statement stmt;
	ResultSet res;

	public Conexion() {

	}
	
	

	public Connection initializeDatabase() throws SQLException, ClassNotFoundException {
		// Initialize all the information regarding
		// Database Connection
		String dbDriver = "com.mysql.jdbc.Driver";
		String dbURL = "jdbc:mysql://localhost:3306/";
		// Database name to access
		String dbName = "publivisionbd";
		String dbUsername = "root";
		String dbPassword = "";

		Class.forName(dbDriver);
		Connection con = DriverManager.getConnection(dbURL + dbName, dbUsername, dbPassword);

		return con;
	}

	public ResultSet getResult(String sql, Connection conn) throws SQLException {
		this.conn = conn;
		try {
			stmt = conn.createStatement();
			res = stmt.executeQuery(sql);
		} finally {
			try {
				if (res != null) {
					res.close();
				}
			} finally {
				try {
					if (stmt != null) {
						stmt.close();
					}
				} finally {
					if (conn != null) {
						conn.close();
					}
				}
			}
		}
		return res;
	}

	public List<ProductoVO> getProductList(String sql, Connection conn) throws SQLException {
		this.conn = conn;
		List<ProductoVO> lstProducto = new ArrayList<>();
		ProductoVO producto = new ProductoVO();
		ProductMapper mapper = new ProductMapper("1");
		try {
			stmt = conn.createStatement();
			res = stmt.executeQuery(sql);

			while (res.next()) {
				producto = mapper.mapRow(res);
				lstProducto.add(producto);
			}
			return lstProducto;
		} finally {
			try {
				if (res != null) {
					res.close();
				}
			} finally {
				try {
					if (stmt != null) {
						stmt.close();
					}
				} finally {
					if (conn != null) {
						conn.close();
					}
				}
			}
		}
	}

	public CotizacionVO getCotList2(String sql, Connection conn, String cantidad) throws SQLException {
		this.conn = conn;
		CotizacionVO cotizacion = new CotizacionVO();
		ProductoVO producto = new ProductoVO();
		ProductMapper mapper = new ProductMapper("1");
		try {
			stmt = conn.createStatement();
			res = stmt.executeQuery(sql);

			while (res.next()) {
				producto = mapper.mapRow(res);
				cotizacion.setProducto(producto);
				cotizacion.setCantidad(Integer.parseInt(cantidad));
			}

			return cotizacion;
		} finally {
			try {
				if (res != null) {
					res.close();
				}
			} finally {
				try {
					if (stmt != null) {
						stmt.close();
					}
				} finally {
					if (conn != null) {
						conn.close();
					}
				}
			}
		}
	}

	public int doUpdate(String sql, Connection conn) throws SQLException {

		this.conn = conn;
		int resultado = 0;
		try {
			stmt = conn.createStatement();
			resultado = stmt.executeUpdate(sql);
		} finally {
			try {
				if (res != null) {
					res.close();
				}
			} finally {
				try {
					if (stmt != null) {
						stmt.close();
					}
				} finally {
					if (conn != null) {
						conn.close();
					}
				}
			}
		}
		return resultado;
	}

	public int doInsert(String sql, Connection conn) throws SQLException {

		this.conn = conn;
		int resultado = 0;
		stmt = conn.createStatement();
		resultado = stmt.executeUpdate(sql);
		return resultado;
	}

	public ClienteVO getCustomer(String sql, Connection conn) throws SQLException {
		ClienteVO cliente = null;
		CustomerMapper mapper = new CustomerMapper("1");
		this.conn = conn;
		stmt = conn.createStatement();
		res = stmt.executeQuery(sql);
		if (res.next()) {
			cliente = mapper.mapRow(res);
		}
		return cliente;
	}
}
