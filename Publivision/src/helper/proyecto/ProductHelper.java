package helper.proyecto;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import vo.proyecto.ProductoVO;

public class ProductHelper {
	
	public ProductHelper() {
		
	}

	public PrintWriter chargeProduct(List<ProductoVO> lstProducts, HttpServletResponse response) throws IOException {

		response.setContentType("text/html; charset=iso-8859-1");
		PrintWriter out = response.getWriter();
		
		int first = 1;
		
		if (null != lstProducts && !lstProducts.isEmpty()) {
			for(ProductoVO producto : lstProducts) {
				if(first != 1) {
					out.println("-");
				}
				out.println(producto.getIdImagenProduct()+"-");
				out.println(producto.getNombreProducto()+"-");
				out.println(producto.getDescripcionProduct()+"-");
				out.println(producto.getIdProduct());
				first = 0;
			}
		}else {
			out = null;
		}

		return out;
	}
	
	public PrintWriter grillaFinal(List<ProductoVO> lstProducts, HttpServletResponse response) throws IOException {
		
		response.setContentType("text/html; charset=iso-8859-1");
		PrintWriter out = response.getWriter();
		
int first = 1;
		
		if (null != lstProducts && !lstProducts.isEmpty()) {
			for(ProductoVO producto : lstProducts) {
				if(first != 1) {
					out.println("-");
				}
				out.println(producto.getIdProduct()+"-");
				out.println(producto.getNombreProducto()+"-");
				out.println(producto.getTipoProduct()+"-");
				out.println(producto.getAltoProduct()+"x"+producto.getAnchoProduct());
				first = 0;
			}
		}else {
			out = null;
		}
		
		return out;
	}
	
	public StringBuilder armarQueryGrilla(String idProducts) {
		StringBuilder query = new StringBuilder();
		String[] indicadores = idProducts.split(",");
		int first = 0;
		
		query.append("SELECT * FROM PRODUCTO WHERE ID_PRODUCTO IN (");
		for(String id : indicadores) {
			if(first == 0) {
				query.append(" "+id+" ");
				first = 1;
			}else {
				query.append(", "+id+" ");
			}
		}
		query.append(")");
		return query;
	}

	public StringBuilder queryCotizacionProductos(String id) {
		StringBuilder query = new StringBuilder();
		
		
		
		return null;
	}
}
