package helper.proyecto;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import vo.proyecto.ClienteVO;
import vo.proyecto.DireccionVO;

public class CotizacionHelper {

	public CotizacionHelper() {

	}

	public PrintWriter responseClientData(ClienteVO cliente, HttpServletResponse response) throws IOException {

		response.setContentType("text/html; charset=iso-8859-1");
		PrintWriter out = response.getWriter();

		if (null != cliente && 0 != cliente.getRutCliente()) {
			out.println(cliente.getNombreRazon() + " ");
			out.println(cliente.getApellidos() + " ");
			out.println(cliente.getDireccion().getPais() + " ");
			out.println(cliente.getDireccion().getCiudad() + " ");
			out.println(cliente.getDireccion().getNombreCalle() + " ");
			out.println(cliente.getDireccion().getNumeroCalle() + " ");
			out.println(cliente.getCorreo());
		} else {
			out = null;
		}

		return out;
	}

	public PrintWriter responseCotizacion(int result, String resultado, HttpServletResponse response) throws IOException {

		response.setContentType("text/html; charset=iso-8859-1");
		PrintWriter out = response.getWriter();

		if(result != 0 && resultado != null) {
			out.println("La cotizacion se ha realizado correctamente!");
		}else {
			out.println("");
		}
		return out;
	}

	public ClienteVO mapRegisterCustomer(HttpServletRequest request) {
		ClienteVO cliente = new ClienteVO();

		cliente.setNombreRazon(request.getParameter("nombre"));
		cliente.setApellidos(request.getParameter("apellidos"));
		cliente.setCorreo(request.getParameter("correo"));
		String rut = request.getParameter("rut");
		String rut2 = rut.replace(".", "");
		String rut3 = rut2.replace("-", "");
		cliente.setRutCliente(Integer.parseInt(rut3));

		if (request.getParameter("empresa") != "0") {
			cliente.setFlagCorporate(Integer.parseInt(request.getParameter("empresa")));
			cliente.setGiro(request.getParameter("giro"));
		}

		DireccionVO direccion = new DireccionVO();
		direccion.setCiudad(request.getParameter("ciudad"));
		direccion.setPais(request.getParameter("pais"));
		direccion.setRegion(request.getParameter("region"));
		direccion.setNombreCalle(request.getParameter("calle"));
		direccion.setNumeroCalle(Integer.parseInt(request.getParameter("numeroCalle")));
		direccion.setTipoDireccion(request.getParameter("tipoDireccion"));
		if (direccion.getTipoDireccion() == "Complejo") {
			direccion.setNombreComplejo(request.getParameter("nombreComplejo"));
			direccion.setPiso(Integer.parseInt(request.getParameter("piso")));
			direccion.setComentarioRef(request.getParameter("comentario"));
		}

		cliente.setDireccion(direccion);

		return cliente;
	}

}
