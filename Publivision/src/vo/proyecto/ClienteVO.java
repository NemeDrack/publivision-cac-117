package vo.proyecto;

public class ClienteVO {
	
	private int rutCliente;
	
	private String correo;
	
	private int flagCorporate;
	
	private int flagDestacado;
	
	private String giro;
	
	private int idDireccion;
	
	private String nombreRazon;
	
	private DireccionVO direccion;
	
	private String apellidos;
	
	
	
	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public DireccionVO getDireccion() {
		return direccion;
	}

	public void setDireccion(DireccionVO direccion) {
		this.direccion = direccion;
	}

	public int getRutCliente() {
		return rutCliente;
	}

	public void setRutCliente(int rutCliente) {
		this.rutCliente = rutCliente;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public int getFlagCorporate() {
		return flagCorporate;
	}

	public void setFlagCorporate(int flagCorporate) {
		this.flagCorporate = flagCorporate;
	}

	public int getFlagDestacado() {
		return flagDestacado;
	}

	public void setFlagDestacado(int flagDestacado) {
		this.flagDestacado = flagDestacado;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public int getIdDireccion() {
		return idDireccion;
	}

	public void setIdDireccion(int idDireccion) {
		this.idDireccion = idDireccion;
	}

	public String getNombreRazon() {
		return nombreRazon;
	}

	public void setNombreRazon(String nombreRazon) {
		this.nombreRazon = nombreRazon;
	}
	
	

}
