package vo.proyecto;

import java.util.List;

public class CotizacionVO {
	
	private ProductoVO producto;
	
	private int cantidad;

	public ProductoVO getProducto() {
		return producto;
	}

	public void setProducto(ProductoVO producto) {
		this.producto = producto;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	
}
