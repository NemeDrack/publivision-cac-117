/**
 * 
 */
package vo.proyecto;

/**
 * @author rzunigas
 *
 */
public class FormularioVO {
	
	private String nombre;
	private String apellido;
	private String rut;
	private String numero;
	private String mail;
	private String comentario;
	private String sCOD_SUCCESS="Se ha insertado correctamente el registro";

	public FormularioVO(){
		
	}

	/**
	 * @return the nombre
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @param nombre the nombre to set
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * @return the apellido
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * @param apellido the apellido to set
	 */
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	/**
	 * @return the rut
	 */
	public String getRut() {
		return rut;
	}

	/**
	 * @param rut the rut to set
	 */
	public void setRut(String rut) {
		this.rut = rut;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the comentario
	 */
	public String getComentario() {
		return comentario;
	}

	/**
	 * @param comentario the comentario to set
	 */
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	/**
	 * @return the sCOD_SUCCESS
	 */
	public String getsCOD_SUCCESS() {
		return sCOD_SUCCESS;
	}

	/**
	 * @param sCOD_SUCCESS the sCOD_SUCCESS to set
	 */
	public void setsCOD_SUCCESS(String sCOD_SUCCESS) {
		this.sCOD_SUCCESS = sCOD_SUCCESS;
	}
	
}
