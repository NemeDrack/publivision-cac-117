package vo.proyecto;

public class ProductoVO {

	private String idProduct;
	private String descripcionProduct;
	private String idImagenProduct;
	private int stockProduct;
	private int retenidoProduct;
	private String tipoProduct;
	private boolean destacadoProduct;
	private String anchoProduct;
	private String altoProduct;
	private String colorProduct;
	private int costoProduct;
	private String nombreProducto;
	
	public String getNombreProducto() {
		return nombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}
	public String getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}
	public String getDescripcionProduct() {
		return descripcionProduct;
	}
	public void setDescripcionProduct(String descripcionProduct) {
		this.descripcionProduct = descripcionProduct;
	}
	public String getIdImagenProduct() {
		return idImagenProduct;
	}
	public void setIdImagenProduct(String idImagenProduct) {
		this.idImagenProduct = idImagenProduct;
	}
	public int getStockProduct() {
		return stockProduct;
	}
	public void setStockProduct(int stockProduct) {
		this.stockProduct = stockProduct;
	}
	public int getRetenidoProduct() {
		return retenidoProduct;
	}
	public void setRetenidoProduct(int retenidoProduct) {
		this.retenidoProduct = retenidoProduct;
	}
	public String getTipoProduct() {
		return tipoProduct;
	}
	public void setTipoProduct(String tipoProduct) {
		this.tipoProduct = tipoProduct;
	}
	public boolean isDestacadoProduct() {
		return destacadoProduct;
	}
	public void setDestacadoProduct(boolean destacadoProduct) {
		this.destacadoProduct = destacadoProduct;
	}
	public String getAnchoProduct() {
		return anchoProduct;
	}
	public void setAnchoProduct(String anchoProduct) {
		this.anchoProduct = anchoProduct;
	}
	public String getAltoProduct() {
		return altoProduct;
	}
	public void setAltoProduct(String altoProduct) {
		this.altoProduct = altoProduct;
	}
	public String getColorProduct() {
		return colorProduct;
	}
	public void setColorProduct(String colorProduct) {
		this.colorProduct = colorProduct;
	}
	public int getCostoProduct() {
		return costoProduct;
	}
	public void setCostoProduct(int costoProduct) {
		this.costoProduct = costoProduct;
	}
}
