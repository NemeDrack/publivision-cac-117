package servlet.proyecto;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import conexion.bbdd.Conexion;
import helper.proyecto.CotizacionHelper;
import helper.proyecto.ProductHelper;
import managed.proyecto.CotizacionManaged;
import managed.proyecto.ProductoManaged;
import vo.proyecto.ClienteVO;
import vo.proyecto.CotizacionVO;
import vo.proyecto.ProductoVO;

/**
 * Servlet implementation class CotizacionServlet
 */
@WebServlet("/CotizacionServlet")
public class CotizacionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	String idProduct;
	
	String rut;
	
	String validation;
	
	String nombre;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getValidation() {
		return validation;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public CotizacionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.rut = request.getParameter("rut");
		this.validation = request.getParameter("validacion");
		this.nombre = request.getParameter("nombre");
		request.setAttribute("rut", "red");
		
		ProductoManaged managed = new ProductoManaged();
		ClienteVO cliente = managed.getClient(this.rut);
		if(null != cliente) {
			validation = "1";
		}
		
		CotizacionHelper helper = new CotizacionHelper();
        PrintWriter out = helper.responseClientData(cliente, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int result;
		String resultado = "";
		
		CotizacionHelper helperCustomer = new CotizacionHelper();
		ProductoManaged managedProduct = new ProductoManaged();
		CotizacionManaged managedCotizacion = new CotizacionManaged();
		String idRaw = request.getParameter("codes");
		
		ClienteVO cliente = helperCustomer.mapRegisterCustomer(request);
		String rut = String.valueOf(cliente.getRutCliente());
		ClienteVO clienteValidate = managedProduct.getClient(rut);
		if(clienteValidate == null) {
			resultado = managedCotizacion.ingresarCliente(cliente);
		}
		
		List<CotizacionVO> lstCotizacion = managedProduct.realizarCruce(idRaw);
		
		result = managedCotizacion.doCotizacion(lstCotizacion, cliente);
		
		
		
		PrintWriter out = helperCustomer.responseCotizacion(result, resultado, response);
		
	}

}
