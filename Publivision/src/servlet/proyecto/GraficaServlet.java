package servlet.proyecto;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import helper.proyecto.ProductHelper;
import managed.proyecto.ProductoManaged;
import vo.proyecto.ProductoVO;

/**
 * Servlet implementation class GraficaServlet
 */
@WebServlet("/GraficaServlet")
public class GraficaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	String tipoProduct;
    
    public String getTipoProduct() {
		return tipoProduct;
	}

	public void setTipoProduct(String tipoProduct) {
		this.tipoProduct = tipoProduct;
	}
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GraficaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		ProductoManaged managed = new ProductoManaged();
		this.tipoProduct = request.getParameter("tipo");
		
		List<ProductoVO> lstProduct = managed.consultProducts(this.tipoProduct);
		ProductHelper helper = new ProductHelper();
		PrintWriter out = helper.chargeProduct(lstProduct, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
