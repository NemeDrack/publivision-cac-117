package servlet.proyecto;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import helper.proyecto.ProductHelper;
import managed.proyecto.ProductoManaged;
import vo.proyecto.ProductoVO;

/**
 * Servlet implementation class ProductServlet
 */
@WebServlet("/ProductServlet")
public class ProductServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	String idProduct;
	
	String productIdSession;
	
    public String getProductIdSession() {
		return productIdSession;
	}

	public void setProductIdSession(String productIdSession) {
		this.productIdSession = productIdSession;
	}

	public String getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(String idProduct) {
		this.idProduct = idProduct;
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public ProductServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session= request.getSession(true);
		this.idProduct = request.getParameter("productId");
		
		if(null != session.getAttribute("productId")) {
			this.productIdSession = session.getAttribute("productId").toString()+","+this.idProduct;
			session.removeAttribute("productId");
			this.idProduct = this.productIdSession;
		}
		session.setAttribute("productId", this.idProduct);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session= request.getSession(true);
		if(null != session.getAttribute("productId")) {
			this.idProduct = session.getAttribute("productId").toString();
			ProductHelper helper = new ProductHelper();
			ProductoManaged managed = new ProductoManaged();
			List<ProductoVO> lstProducts = managed.cargaGrilla(idProduct);
			PrintWriter out = helper.grillaFinal(lstProducts, response);
		}
	}
}
