package managed.proyecto;

import vo.proyecto.ClienteVO;
import vo.proyecto.CotizacionVO;
import vo.proyecto.ProductoVO;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import conexion.bbdd.Conexion;
import helper.proyecto.ProductHelper;

public class ProductoManaged {

	ProductHelper helper = new ProductHelper();

	public ProductoManaged() {
		super();
	}

	public int insertProduct(ProductoVO producto) {
		Connection conn;
		int resp = 0;
		Conexion dataBase = new Conexion();
		StringBuilder query = new StringBuilder();
		query.append(" INSERT INTO `producto` ");
		query.append(" (`ID_PRODUCTO`, `DESCRIPCION`, `STOCK`, `RETENIDO`, ");
		query.append(" `TIPO_PRODUCTO`, `FLAG_DESTACADO`, `ID_IMAGEN`, `ANCHO`, `ALTO`, `COLOR`, `COSTO UNITARIO`) ");
		query.append(" VALUES ('" + producto.getIdProduct() + "', '" + producto.getDescripcionProduct() + "', '"
				+ producto.getStockProduct() + "', '" + producto.getRetenidoProduct() + "', ");
		query.append(" '" + producto.getTipoProduct() + "', '0', '" + producto.getIdImagenProduct() + "', '"
				+ producto.getAnchoProduct() + "', '" + producto.getAltoProduct() + "', '" + producto.getColorProduct()
				+ "', '" + producto.getCostoProduct() + "')");

		try {
			conn = dataBase.initializeDatabase();
			resp = dataBase.doInsert(query.toString(), conn);

		} catch (Exception e) {

		}
		return resp;
	}

	public int updateProduct(ProductoVO producto) {
		Connection conn;
		int resp = 0;
		Conexion dataBase = new Conexion();
		StringBuilder query = new StringBuilder();
		query.append(" UPDATE producto SET ");
		query.append(" SET ");
		query.append(" DESCRIPCION = '" + producto.getDescripcionProduct() + "', ");
		query.append(" TIPO_PRODUCTO = '" + producto.getTipoProduct() + "', ");
		query.append(" ID_IMAGEN = '" + producto.getIdImagenProduct() + "', ");
		query.append(" ANCHO = '" + producto.getAnchoProduct() + "', ");
		query.append(" ALTO = '" + producto.getAltoProduct() + "', ");
		query.append(" COLOR = '" + producto.getColorProduct() + "', ");
		query.append(" COSTO UNITARIO = '" + producto.getCostoProduct() + "' ");
		query.append(" WHERE ID_PRODUCTO = " + producto.getIdProduct());

		try {
			conn = dataBase.initializeDatabase();
			resp = dataBase.doUpdate(query.toString(), conn);
		} catch (Exception e) {

		}
		return resp;
	}

	public List<ProductoVO> consultProducts(String tipo) {
		Connection conn;
		List<ProductoVO> lstProducts = new ArrayList<ProductoVO>();
		Conexion dataBase = new Conexion();
		StringBuilder query = new StringBuilder();
		query.append("SELECT * FROM PRODUCTO WHERE TIPO_PRODUCTO = '" + tipo + "'");

		try {
			conn = dataBase.initializeDatabase();
			lstProducts = dataBase.getProductList(query.toString(), conn);

		} catch (Exception e) {

		}
		return lstProducts;
	}

	public ClienteVO getClient(String rutCliente) {
		ClienteVO cliente = new ClienteVO();
		Connection conn;
		Conexion dataBase = new Conexion();
		StringBuilder query = new StringBuilder();
		query.append(
				"SELECT * FROM CLIENTE CL JOIN DIRECCION DI ON CL.ID_DIRECCION = DI.ID_DIRECCION WHERE ID_CLIENTE = '"
						+ rutCliente + "'");

		try {
			conn = dataBase.initializeDatabase();
			cliente = dataBase.getCustomer(query.toString(), conn);
		} catch (Exception e) {
			String error = e.getMessage();
			cliente = null;
		}
		return cliente;
	}

//	public int insertDetalleCotizacion(ProductoVO producto) {
//		CotizacionVO cotizacion = new CotizacionVO();
//		Connection conn;
//		Conexion dataBase = new Conexion();
//		int resp = 0;
//		try {
//			for (ProductoVO productoLst : cotizacion.getProducto()) {
//				StringBuilder query = new StringBuilder();
//				query.append(" INSERT INTO detalle_cotizacion ");
//				query.append(" (ID_DETALLE, ID_PRODUCTO, ID_SERVICIO) ");
//				query.append(" VALUES (1, " + productoLst.getIdProduct() + ", 1) ");
//
//				conn = dataBase.initializeDatabase();
//				resp = dataBase.doInsert(query.toString(), conn);
//			}
//		} catch (Exception e) {
//
//		}
//		return resp;
//	}

	public List<ProductoVO> cargaGrilla(String idProducts) {
		Connection conn;
		List<ProductoVO> lstProducts = new ArrayList<ProductoVO>();
		Conexion dataBase = new Conexion();
		StringBuilder query = helper.armarQueryGrilla(idProducts);

		try {
			conn = dataBase.initializeDatabase();
			lstProducts = dataBase.getProductList(query.toString(), conn);

		} catch (Exception e) {

		}

		CotizacionVO cotizacion = new CotizacionVO();
		return lstProducts;
	}

	public List<CotizacionVO> realizarCruce(String idLong) {
		Connection conn;
		Conexion dataBase = new Conexion();
		CotizacionVO cotizacion = new CotizacionVO();
		List<CotizacionVO> lstCotizacion = new ArrayList<CotizacionVO>();
		String[] duplas = idLong.split("-");
		String[] tripleca;

		for (String conjunto : duplas) {
			tripleca = conjunto.split(",");
			StringBuilder query = new StringBuilder();
			query.append("SELECT * FROM PRODUCTO WHERE ID_PRODUCTO = " + tripleca[0]);
			
			try {
				conn = dataBase.initializeDatabase();
				cotizacion = dataBase.getCotList2(query.toString(), conn, tripleca[1]);
				lstCotizacion.add(cotizacion);
			} catch (Exception e) {

			}
		}

		return lstCotizacion;
	}

}
