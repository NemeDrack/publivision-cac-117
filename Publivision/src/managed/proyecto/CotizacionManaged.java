package managed.proyecto;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import conexion.bbdd.Conexion;
import mapper.proyecto.CotizacionMapper;
import vo.proyecto.ClienteVO;
import vo.proyecto.CotizacionVO;
import vo.proyecto.ProductoVO;

public class CotizacionManaged {

	Connection conn;
	Statement stmt;
	ResultSet res;

	public CotizacionManaged() {
		super();
	}

	public int doCotizacion(List<CotizacionVO> lstCotizacion, ClienteVO cliente) {
		int res = 0;
		double montoSuma = 0;
		double montoNeto = 0;
		double montoTotal;
		double montoIva;
		for (CotizacionVO cotizacion : lstCotizacion) {
			montoSuma = cotizacion.getProducto().getCostoProduct() * cotizacion.getCantidad();
			montoNeto = montoNeto + montoSuma;
		}
		montoIva = montoNeto * 0.19;
		montoTotal = montoNeto + montoIva;
		try {
			res = insertCotizacion(montoNeto, montoIva, montoTotal, cliente);
			res = insertDetalleCotizacion(lstCotizacion);

		} catch (Exception e) {
			res = 0;
		}

		return res;
	}

	public int insertCotizacion(double montoNeto, double montoIva, double montoTotal, ClienteVO cliente)
			throws SQLException, ClassNotFoundException {
		Conexion dataBase = new Conexion();
		Connection conn = dataBase.initializeDatabase();
		int resp = 0;

		StringBuilder query = new StringBuilder();

		query.append(" INSERT INTO cotizacion ");
		query.append(
				" (`ID_CLIENTE`, `FECHA_ENTREGA`, `FECHA_CREACION`, `MONTO_NETO`, `MONTO_IVA`, `MONTO_TOTAL`) VALUES (");
		query.append(" " + cliente.getRutCliente() + ", ");
		query.append(" NOW(), ");
		query.append(" NOW(), ");
		query.append(" '" + montoNeto + "', ");
		query.append(" '" + montoIva + "', ");
		query.append(" '" + montoTotal + "'");
		query.append("  )");

		resp = dataBase.doInsert(query.toString(), conn);

		return resp;
	}

	public int insertDetalleCotizacion(List<CotizacionVO> lstCotizacion) throws ClassNotFoundException, SQLException {
		Conexion dataBase = new Conexion();
		Connection conn = dataBase.initializeDatabase();
		int resp = 0;

		for (CotizacionVO cotizacion : lstCotizacion) {

			StringBuilder query = new StringBuilder();
			query.append(" INSERT INTO detalle_cotizacion (");
			query.append(" `ID_COTIZACION`, `ID_PRODUCTO`) VALUES ( ");
			query.append(" (SELECT MAX(ID_COTIZACION) FROM cotizacion) , '");
			query.append(cotizacion.getProducto().getIdProduct());
			query.append("' ) ");
			resp = dataBase.doInsert(query.toString(), conn);
		}

		return resp;
	}

	public String ingresarCliente(ClienteVO cliente) {
		String resultado = "";
		int res = 0;
		try {
			res = insertAdress(cliente);
			res = insertCustomer(cliente);
		} catch (SQLException e) {
			resultado = "Excepcion SQL";
		} catch (ClassNotFoundException e) {
			resultado = "Excepcion Conexion";
		}

		if (res > 0) {
			resultado = "Ingreso correcto";
		}
		return resultado;
	}

	public int insertAdress(ClienteVO cliente) throws ClassNotFoundException, SQLException {
		Conexion dataBase = new Conexion();
		Connection conn = dataBase.initializeDatabase();
		int resp = 0;

		StringBuilder query = new StringBuilder();
		query.append(" INSERT INTO direccion ( ");
		query.append(" `PAIS`, `REGION`, `CIUDAD`, `NOMBRE_CALLE`, `NUMERO_CALLE` ");

		if (null == cliente.getDireccion().getTipoDireccion()) {
			query.append(", `TIPO_DIRECCION`, `PISO`, `NOMBRE_COMPLEJO`, `COMENTARIO_REFERENCIA` ");
		}
		query.append(" ) VALUES ( ");
		query.append(" '" + cliente.getDireccion().getPais() + "', ");
		query.append(" '" + cliente.getDireccion().getRegion() + "', ");
		query.append(" '" + cliente.getDireccion().getCiudad() + "', ");
		query.append(" '" + cliente.getDireccion().getNombreCalle() + "', ");
		query.append(" '" + cliente.getDireccion().getNumeroCalle() + "'");
		if (null == cliente.getDireccion().getTipoDireccion()) {
			query.append(" , " + cliente.getDireccion().getTipoDireccion());
			query.append(" , " + cliente.getDireccion().getPiso());
			query.append(" , " + cliente.getDireccion().getNombreCalle());
			query.append(" , " + cliente.getDireccion().getComentarioRef());
		}
		query.append(" ) ");

		resp = dataBase.doInsert(query.toString(), conn);

		return resp;
	}

	public int insertCustomer(ClienteVO cliente) throws ClassNotFoundException, SQLException {
		Conexion dataBase = new Conexion();
		Connection conn = dataBase.initializeDatabase();
		int resp = 0;

		StringBuilder query = new StringBuilder();
		query.append(" INSERT INTO cliente (");
		query.append(
				" `ID_CLIENTE`, `ID_DIRECCION`, `NOMBRE_RAZON`, `APELLIDOS`, `CORREO`, `GIRO`, `FLAG_CORPORATE`, `FLAG_DESTACADO`) VALUES ( ");
		query.append(" '" + cliente.getRutCliente() + "', ");
		query.append(" (SELECT MAX(ID_DIRECCION) FROM direccion) , ");
		query.append(" '" + cliente.getNombreRazon() + "', ");
		query.append(" '" + cliente.getApellidos() + "', ");
		query.append(" '" + cliente.getCorreo() + "', ");
		query.append(" '" + cliente.getGiro() + "', ");
		query.append(" '" + cliente.getFlagCorporate() + "', ");
		query.append(" '" + cliente.getFlagDestacado() + "'");
		query.append(" ) ");

		resp = dataBase.doInsert(query.toString(), conn);

		return resp;
	}

}
