<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Publivision</title>

<!-- Bootstrap core CSS -->
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="vendor/bootstrap/js/script.js"></script>

<!-- Custom fonts for this template -->
<link
	href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i"
	rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/business-casual.min.css" rel="stylesheet">



<style>
#snackbar {
	visibility: hidden;
	min-width: 250px;
	margin-left: -125px;
	background-color: rgba(116, 171, 4, .9);
	color: #fff;
	text-align: center;
	border-radius: 5px;
	border-color: black;
	padding: 16px;
	position: fixed;
	z-index: 1;
	left: 50%;
	bottom: 30px;
	font-size: 17px;
}

#snackbar.show {
	visibility: visible;
	-webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
	animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@
-webkit-keyframes fadein {
	from {bottom: 0;
	opacity: 0;
}

to {
	bottom: 30px;
	opacity: 1;
}

}
@
keyframes fadein {
	from {bottom: 0;
	opacity: 0;
}

to {
	bottom: 30px;
	opacity: 1;
}

}
@
-webkit-keyframes fadeout {
	from {bottom: 30px;
	opacity: 1;
}

to {
	bottom: 0;
	opacity: 0;
}

}
@
keyframes fadeout {
	from {bottom: 30px;
	opacity: 1;
}

to {
	bottom: 0;
	opacity: 0;
}
}
</style>

</head>

<body>

	<h1 class="site-heading text-center text-white d-none d-lg-block">
		<img class="intro-img mb-3 mb-lg-0 rounded site-heading-lower"
			height="auto" width="50%" src="img/mainlogo.png" alt="">
	</h1>
	
	

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
		<div class="container">
			<a
				class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none"
				href="#"> <img src="img/OJO.png" width="45" alt=""
				class="d-inline-block align-middle mr-2"> <span
				class="text-uppercase font-weight-bold">Publivision</span>
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarResponsive" aria-controls="navbarResponsive"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav mx-auto">
					<li class="nav-item px-lg-4"><a
						class="nav-link text-uppercase text-expanded" href="index.jsp">Principal
							<span class="sr-only">(current)</span>
					</a></li>
					<li class="nav-item px-lg-4"><a
						class="nav-link text-uppercase text-expanded" href="about.jsp">�Quienes
							Somos?</a></li>
					<li class="nav-item px-lg-4"><a
						class="nav-link text-uppercase text-expanded" href="products.jsp">Productos</a>
					</li>
					<li class="nav-item active px-lg-4"><a
						class="nav-link text-uppercase text-expanded"
						href="cotizadorPage.jsp">Cotizador</a></li>
					<li class="nav-item px-lg-4"><a
						class="nav-link text-uppercase text-expanded" href="store.jsp">Contacto</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	<section class="page-section ml-10 mr-10">
		<div class="container">
			<div class="about-heading-content">
				<div class="row">
					<div class="col-xl-9 col-lg-10 mx-auto">
						<div class="bg-faded rounded p-5">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<div class="well well-sm">
											<form action="${pageContext.request.contextPath}/CotizacionServlet" method="post" class="form-inline">
												<div class="form-group mb-2">
													<label for="labelRut" class="sr-only"> </label> <input
														type="text" readonly class="form-control-plaintext"
														id="labelRut" value="    ">
												</div>
												<div class="form-group mx-sm-3 mb-2">
													<label for="formRut" class="sr-only">Ingresar Rut</label> <input
														type="text" class="form-control" id="formRut" name="rut"
														placeholder="Ingresar Rut">
												</div>
												<input type="hidden" id="hdnNombre" name="hdnVarNombre" value="nada">
												<button onclick="validateRut(document.getElementById('formRut').value);" type="button" class="btn btn-primary mb-2" id="botonTest">Comprobar</button>
											</form>
											<form id="formularioDatos" class="d-none">
												<br> <br>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputNombre">Nombre</label> <input type="text"
															class="form-control" id="inputNombre"
															placeholder="Nombre" value="${nombre}">
													</div>
													<div class="form-group col-md-6">
														<label for="inputApellidos">Apellidos</label> <input
															type="text" class="form-control" id="inputApellidos"
															placeholder="Apellidos">
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputEmail">Email</label> <input type="email"
															class="form-control" id="inputEmail" placeholder="Email">
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-4">
														<label for="inputPais">Pais</label> <input type="text"
															class="form-control" id="inputPais" placeholder="Pais">
													</div>
													<div class="form-group col-md-4">
														<label for="inputState">Region</label> <select
															id="inputState" class="form-control">
															<option>Elige...</option>
															<option>I Region de Tarapaca</option>
															<option>II Region de Antofagasta</option>
															<option>III Region de Atacama</option>
															<option>IV Region de Coquimbo</option>
															<option>V Region de Valpara�so</option>
															<option>VI Region del Libertador General Bernardo OHiggins</option>
															<option>VII Region del Maule</option>
															<option>VIII Region del Biob�o</option>
															<option selected>IX Region de La Araucan�a</option>
															<option>X Region de Los Lagos</option>
															<option>XI Region Ays�n del General Carlos Ibanez del Campo</option>
															<option>XII Region de Magallanes y Antartica Chilena</option>
															<option>Region Metropolitana de Santiago</option>
															<option>XIV Region de Los R�os</option>
															<option>XV Region de Arica y Parinacota</option>
															<option>XVI Region de �uble</option>
														</select>
													</div>
													<div class="form-group col-md-4">
														<label for="inputCiudad">Ciudad</label> <input type="text"
															class="form-control" id="inputCiudad"
															placeholder="Ciudad">
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="inputCalle">Calle</label> <input type="text"
															class="form-control" id="inputCalle" placeholder="Calle">
													</div>
													<div class="form-group col-md-4">
														<label for="inputNumeroCalle">Numero</label> <input
															type="text" class="form-control" id="inputNumeroCalle"
															placeholder="Numero de Direccion">
													</div>
													<div class="form-group col-md-2">
														<label for="optionComplex">�Edificio?</label> <select
															id="optionComplex" class="form-control" onchange="botonComplejo();">
															<option selected>Elige...</option>
															<option value="si">Si</option>
															<option value="no">No</option>
														</select>
													</div>
												</div>
												<div class="form-row d-none" id="complejo1">
													<div class="form-group col-md-2">
														<label for="inputPiso">Piso</label> <input type="text"
															class="form-control" id="inputPiso" placeholder="Piso">
													</div>
													<div class="form-group col-md-6">
														<label for="inputNombreEdificio">Nombre Edif.</label> <input
															type="text" class="form-control" id="inputNombreEdificio"
															placeholder="Nombre Edificio">
													</div>
												</div>
												<div class="form-row d-none" id="complejo2">
													<div class="form-group col-md-12">
														<label for="inputComentario">Comentario de
															Referencia</label> <input type="text" class="form-control"
															id="inputComentario" placeholder="Comentario...">
													</div>
												</div>
												<div class="form-group">
													<div class="form-check">
														<input class="form-check-input" type="checkbox"
															id="gridCheck"> <label class="form-check-label"
															for="gridCheck"> Confirmar Datos </label>
													</div>
												</div>
												<ul class="list-group list-group-horizontal" id="topCotList">
													<li
														class="list-group-item col-md-1 list-group-item-success"><b>N�</b></li>
													<li
														class="list-group-item col-md-5 list-group-item-success"><b>Descripcion</b></li>
													<li
														class="list-group-item col-md-2 list-group-item-success"><b>Tipo
														de Producto</b></li>
													<li
														class="list-group-item col-md-2 list-group-item-success"><b>Tama�o</b></li>
													<li
														class="list-group-item col-md-2 list-group-item-success"><b>Cantidad</b></li>
												</ul>
												<div id="grilla">
												</div>
												<br>
												<input type="hidden" id="cantMats" name="hdnVarMats" value="1">
												<button onclick="doCotizacion();" type="button" class="btn btn-primary" id="botonCotizacion">Ingresar</button>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer class="footer text-faded text-center py-5">
		<div class="container">
			<p class="m-0 small">Copyright &copy; Publivision 2019</p>
		</div>
	</footer>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	
	<script>
	$( document ).ready(function() {
		cargaGrilla();
 		});
	</script>

</body>

</html>
