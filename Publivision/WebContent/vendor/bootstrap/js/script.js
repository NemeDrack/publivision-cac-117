function cotizarProducto(productId) {
	$.get('ProductServlet', {
		productId : productId
	}, function(responseText) {
		agregarProducto();
	});
}

function agregarProducto() {
	var x = document.getElementById("snackbar");
	x.className = "show";
	setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
}

function validateRut(rut){
	var validation = 0;
	var nombre = document.getElementById('hdnNombre').value;
	$.get('CotizacionServlet', {
		rut : rut,
		validacion : validation, 
		nombre : nombre
	}, function(data) {
		if(data == ""){
			$('#formularioDatos').removeClass('d-none');
			alert('Cliente no ingresado, favor rellenar el formulario!');
			
			$('#inputNombre').val('');
			$('#inputApellidos').val('');  
			$('#inputPais').val(''); 
			$('#inputCiudad').val(''); 
			$('#inputCalle').val(''); 
			$('#inputNumeroCalle').val(''); 
			$('#inputEmail').val(''); 
		}else{
			var info = data;
			var array = info.split(" ");
			
			var ar = rut.split('');
			var rutFinal = ar[0]+ar[1]+'.'+ar[2]+ar[3]+ar[4]+'.'+ar[5]+ar[6]+ar[7]+'-'+ar[8];
			$('#formRut').val(rutFinal); 
			
				$('#formularioDatos').removeClass('d-none');
				$('#inputNombre').val(array[0]);
				$('#inputApellidos').val(array[1]+' '+array[2]);  
				$('#inputPais').val(array[3]); 
				$('#inputCiudad').val(array[4]); 
				var dir = array[5].replace("-", " ");
				$('#inputCalle').val(dir); 
				$('#inputNumeroCalle').val(array[6]); 
				$('#inputEmail').val(array[7]); 
		}
	});
}

function loadProduct(tipo){
	
	var url;
	
	if(tipo == 'grafica'){
		url = 'GraficaServlet';
	}else if(tipo == 'serigrafia'){
		url = 'SerigrafiaServlet';
	}else if(tipo == 'textil'){
		url = 'TextilServlet';
	}
	
	$.get(url, {
		tipo : tipo,
		carga : 'si'
	}, function(data) {
		if(data == ""){
			alert('No hay productos para mostrar en esta categoria');
		}else{
			var info = data;
			var cadenaModificada = info.split('-');
			var productoCadena = [];
			
			for (var i = 0; i < cadenaModificada.length; i++) {
				
			    productoCadena.push(cadenaModificada[i]+'-'+cadenaModificada[i+1]+'-'+cadenaModificada[i+2]+'-'+cadenaModificada[i+3]);
			    i++;
			    i++;
			    i++;
			}
			
			for (var i = 0; i < productoCadena.length; i++) {
				var product = productoCadena[i].split("-");
				$(".card-columns").append( "<div class='card'><img src='img/"+product[0]+".jpg' class='card-img-top img-thumbnail' alt='Card image cap'><div class='card-body'><h5 class='card-title'>"+product[1]+"</h5><p class='card-text'>"+product[2]+"</p><button onclick='cotizarProducto("+product[3]+")' type='button' class='btn btn-primary'>Cotizar</button></div></div>" );
				}
		}
	});
}

function cargaGrilla(){
	
	$.post('ProductServlet', {
	}, function(data) {
		if(data == ""){
			alert('No se ha cargado ningun producto aun a la Cotizacion!');
		}else{
			var info = data;
			var cadenaModificada = info.split('-');
			var productoCadena = [];
			
			for (var i = 0; i < cadenaModificada.length; i++) {
				
			    productoCadena.push(cadenaModificada[i]+'-'+cadenaModificada[i+1]+'-'+cadenaModificada[i+2]+'-'+cadenaModificada[i+3]);
			    i++;
			    i++;
			    i++;
			}
			
			for (var i = 0; i < productoCadena.length; i++) {
				var product = productoCadena[i].split('-');
				$('#cantMats').val(i+1);
				var algo = document.getElementById('cantMats').value;
//				document.getElementById('cantMats').value = i+1;
			$("#grilla").append("<ul class='list-group list-group-horizontal' id='"+product[0]+"'><li class='list-group-item col-md-1'>"+(i+1)+"</li><li class='list-group-item col-md-5'>"+product[1]+"</li><li class='list-group-item col-md-2'>"+product[2]+"</li><li class='list-group-item col-md-2'>"+product[3]+"</li><li class='list-group-item col-md-2'><input type='text' class='form-control' id='inputCantidadCot"+i+"'placeholder='...'></li><input type='hidden' id='hdnMat"+i+"' value='"+product[0]+"'></ul>");
			}
		}
	});
	
}

function doCotizacion(){
	var rut = document.getElementById('formRut').value;
	var nombre = document.getElementById('inputNombre').value;
	var apellidos = document.getElementById('inputApellidos').value;
	var correo = document.getElementById('inputEmail').value;
	var pais = document.getElementById('inputPais').value;
	var region = document.getElementById('inputState').value;
	var ciudad = document.getElementById('inputCiudad').value;
	var calle = document.getElementById('inputCalle').value;
	var numeroCalle = document.getElementById('inputNumeroCalle').value;
//	var tipoDir = document.getElementById('optionComplex').value;
	var piso = document.getElementById('inputPiso').value;
	var nombreComplejo = document.getElementById('inputNombreEdificio').value;
	var comentarioRef = document.getElementById('inputComentario').value;
	
	var mats = document.getElementById('cantMats').value;
	var id = '';
	var cant = '';
	var elNoArray = '';
	var first = 1;
	
	
	for (var i = 0; i < mats ; i++) {
		
		id = document.getElementById('hdnMat'+i).value;
		cant = document.getElementById('inputCantidadCot'+i).value;
		if(first == 1){
			elNoArray = elNoArray + (id+','+cant);
			first = 0;
		}else{
			elNoArray = elNoArray + ('-'+id+','+cant);
		}
	}
	
	$.post('CotizacionServlet', {
		nombre : nombre,
		apellidos : apellidos,
		correo : correo,
		rut : rut,
		empresa : '0',
		ciudad : ciudad,
		pais : pais,
		region : 'Araucania',
		calle : calle,
		numeroCalle : numeroCalle,
		tipoDireccion : 'Normal',
		codes : elNoArray
	}, function(data) {
		alert(data);
	});
}

function botonComplejo(){
	
	var selectBox = document.getElementById("optionComplex");
	var selectedValue = selectBox.options[selectBox.selectedIndex].value;
	
	if(selectedValue=='si'){
		$('#complejo1').removeClass('d-none');
		$('#complejo2').removeClass('d-none');
	}else if (selectedValue=='no'){
		$('#complejo1').addClass('d-none');
		$('#complejo2').addClass('d-none');
	}
}
