<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Publivision</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/business-casual.min.css" rel="stylesheet">

  </head>

  <body>

    <h1 class="site-heading text-center text-white d-none d-lg-block">
      <img class="intro-img mb-3 mb-lg-0 rounded site-heading-lower" height="auto" width="50%" src="img/mainlogo.png" alt="">
    </h1>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
      <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">
      		<img src="img/OJO.png" width="45" alt="" class="d-inline-block align-middle mr-2">
      		<span class="text-uppercase font-weight-bold">Publivision</span>
      	</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="index.jsp">Principal
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="about.jsp">¿Quienes Somos?</a>
            </li>
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="products.jsp">Productos</a>
            </li>
            <li class="nav-item px-lg-4">
            <a class="nav-link text-uppercase text-expanded" href="Cotizador.jsp">Cotizador</a>
          </li>
            <li class="nav-item active px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="store.jsp">Contacto</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
              <h2 class="section-heading mb-5">
                <span class="section-heading-upper">Acercate!</span>
                <span class="section-heading-lower">Estamos Atendiendo</span>
              </h2>
              <ul class="list-unstyled list-hours mb-5 text-left mx-auto">
                <li class="list-unstyled-item list-hours-item d-flex">
                  Domingo
                  <span class="ml-auto">Cerrado</span>
                </li>
                <li class="list-unstyled-item list-hours-item d-flex">
                  Lunes
                  <span class="ml-auto">10:00 hasta las 19:00</span>
                </li>
                <li class="list-unstyled-item list-hours-item d-flex">
                  Martes
                  <span class="ml-auto">10:00 hasta las 19:00</span>
                </li>
                <li class="list-unstyled-item list-hours-item d-flex">
                  Miercoles
                  <span class="ml-auto">10:00 hasta las 19:00</span>
                </li>
                <li class="list-unstyled-item list-hours-item d-flex">
                  Jueves
                  <span class="ml-auto">10:00 hasta las 19:00</span>
                </li>
                <li class="list-unstyled-item list-hours-item d-flex">
                  Viernes
                  <span class="ml-auto">10:00 hasta las 19:00</span>
                </li>
                <li class="list-unstyled-item list-hours-item d-flex">
                  Sabado
                  <span class="ml-auto">Cerrado</span>
                </li>
              </ul>
              <p class="address mb-5">
                <em>
                  <strong>General Mackenna 898</strong>
                  <br>
                  Esquina San Martin, Temuco Centro
                </em>
              </p>
              <p class="mb-0">
                <small>
                  <em>Comunicarse al siguiente telefono</em>
                </small>
                <br>
                +56 940677231
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <footer class="footer text-faded text-center py-5">
      <div class="container">
        <p class="m-0 small">Copyright &copy; Publivision 2019</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

  <!-- Script to highlight the active date in the hours list -->
  <script>
    $('.list-hours li').eq(new Date().getDay()).addClass('today');
  </script>

</html>
