/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

/**
 *
 * @author rzunigas
 */
public class ClienteVO {

    /**
     * @return the client_idDir
     */
    public int getClient_idDir() {
        return client_idDir;
    }

    /**
     * @param client_idDir the client_idDir to set
     */
    public void setClient_idDir(int client_idDir) {
        this.client_idDir = client_idDir;
    }

    /**
     * @return the cliente_rut
     */
    public String getCliente_rut() {
        return cliente_rut;
    }

    /**
     * @param cliente_rut the cliente_rut to set
     */
    public void setCliente_rut(String cliente_rut) {
        this.cliente_rut = cliente_rut;
    }

    /**
     * @return the cliente_nombre
     */
    public String getCliente_nombre() {
        return cliente_nombre;
    }

    /**
     * @param cliente_nombre the cliente_nombre to set
     */
    public void setCliente_nombre(String cliente_nombre) {
        this.cliente_nombre = cliente_nombre;
    }

    /**
     * @return the cliente_ape
     */
    public String getCliente_ape() {
        return cliente_ape;
    }

    /**
     * @param cliente_ape the cliente_ape to set
     */
    public void setCliente_ape(String cliente_ape) {
        this.cliente_ape = cliente_ape;
    }

    /**
     * @return the cliente_correo
     */
    public String getCliente_correo() {
        return cliente_correo;
    }

    /**
     * @param cliente_correo the cliente_correo to set
     */
    public void setCliente_correo(String cliente_correo) {
        this.cliente_correo = cliente_correo;
    }
    
    private String cliente_rut;
    private String cliente_nombre;
    private String cliente_ape;
    private String cliente_correo;
    private String cliente_calle;
    private String cliente_num;
    private String cliente_region;
    private String cliente_ciudad;
    private String cliente_pais;
    private int client_idDir;

    /**
     * @return the cliente_calle
     */
    public String getCliente_calle() {
        return cliente_calle;
    }

    /**
     * @param cliente_calle the cliente_calle to set
     */
    public void setCliente_calle(String cliente_calle) {
        this.cliente_calle = cliente_calle;
    }

    /**
     * @return the cliente_num
     */
    public String getCliente_num() {
        return cliente_num;
    }

    /**
     * @param cliente_num the cliente_num to set
     */
    public void setCliente_num(String cliente_num) {
        this.cliente_num = cliente_num;
    }

    /**
     * @return the cliente_region
     */
    public String getCliente_region() {
        return cliente_region;
    }

    /**
     * @param cliente_region the cliente_region to set
     */
    public void setCliente_region(String cliente_region) {
        this.cliente_region = cliente_region;
    }

    /**
     * @return the cliente_ciudad
     */
    public String getCliente_ciudad() {
        return cliente_ciudad;
    }

    /**
     * @param cliente_ciudad the cliente_ciudad to set
     */
    public void setCliente_ciudad(String cliente_ciudad) {
        this.cliente_ciudad = cliente_ciudad;
    }

    /**
     * @return the cliente_pais
     */
    public String getCliente_pais() {
        return cliente_pais;
    }

    /**
     * @param cliente_pais the cliente_pais to set
     */
    public void setCliente_pais(String cliente_pais) {
        this.cliente_pais = cliente_pais;
    }
    
}
