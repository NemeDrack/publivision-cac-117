/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

/**
 *
 * @author rzunigas
 */
public class CotizacionVO {

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    

    /**
     * @return the cot_id
     */
    public int getCot_id() {
        return cot_id;
    }

    /**
     * @param cot_id the cot_id to set
     */
    public void setCot_id(int cot_id) {
        this.cot_id = cot_id;
    }

    /**
     * @return the cot_rcliente
     */
    public String getCot_rcliente() {
        return cot_rcliente;
    }

    /**
     * @param cot_rcliente the cot_rcliente to set
     */
    public void setCot_rcliente(String cot_rcliente) {
        this.cot_rcliente = cot_rcliente;
    }

    /**
     * @return the cot_ncliente
     */
    public String getCot_ncliente() {
        return cot_ncliente;
    }

    /**
     * @param cot_ncliente the cot_ncliente to set
     */
    public void setCot_ncliente(String cot_ncliente) {
        this.cot_ncliente = cot_ncliente;
    }

    /**
     * @return the cot_acliente
     */
    public String getCot_acliente() {
        return cot_acliente;
    }

    /**
     * @param cot_acliente the cot_acliente to set
     */
    public void setCot_acliente(String cot_acliente) {
        this.cot_acliente = cot_acliente;
    }

    /**
     * @return the cot_fecha
     */
    public String getCot_fecha() {
        return cot_fecha;
    }

    /**
     * @param cot_fecha the cot_fecha to set
     */
    public void setCot_fecha(String cot_fecha) {
        this.cot_fecha = cot_fecha;
    }

    /**
     * @return the cot_dservicio
     */
    public String getCot_dservicio() {
        return cot_dservicio;
    }

    /**
     * @param cot_dservicio the cot_dservicio to set
     */
    public void setCot_dservicio(String cot_dservicio) {
        this.cot_dservicio = cot_dservicio;
    }

    /**
     * @return the cot_iva
     */
    public Double getCot_iva() {
        return cot_iva;
    }

    /**
     * @param cot_iva the cot_iva to set
     */
    public void setCot_iva(Double cot_iva) {
        this.cot_iva = cot_iva;
    }

    /**
     * @return the cot_neto
     */
    public Double getCot_neto() {
        return cot_neto;
    }

    /**
     * @param cot_neto the cot_neto to set
     */
    public void setCot_neto(Double cot_neto) {
        this.cot_neto = cot_neto;
    }

    /**
     * @return the cot_total
     */
    public Double getCot_total() {
        return cot_total;
    }

    /**
     * @param cot_total the cot_total to set
     */
    public void setCot_total(Double cot_total) {
        this.cot_total = cot_total;
    }
    private int cot_id;
    private String cot_rcliente;
    private String cot_ncliente;
    private String cot_acliente;
    private String cot_fecha;
    private String cot_dservicio;
    private Double cot_iva;
    private Double cot_neto;
    private Double cot_total;
    private String correo;
}
