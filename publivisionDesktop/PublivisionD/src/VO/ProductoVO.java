/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

/**
 *
 * @author rzunigas
 */
public class ProductoVO {

    /**
     * @return the product_id
     */
    public int getProduct_id() {
        return product_id;
    }

    /**
     * @param product_id the product_id to set
     */
    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    /**
     * @return the product_name
     */
    public String getProduct_name() {
        return product_name;
    }

    /**
     * @param product_name the product_name to set
     */
    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    /**
     * @return the product_type
     */
    public String getProduct_type() {
        return product_type;
    }

    /**
     * @param product_type the product_type to set
     */
    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    /**
     * @return the product_dest
     */
    public int getProduct_dest() {
        return product_dest;
    }

    /**
     * @param product_dest the product_dest to set
     */
    public void setProduct_dest(int product_dest) {
        this.product_dest = product_dest;
    }

    /**
     * @return the product_col
     */
    public String getProduct_col() {
        return product_col;
    }

    /**
     * @param product_col the product_col to set
     */
    public void setProduct_col(String product_col) {
        this.product_col = product_col;
    }

    /**
     * @return the product_h
     */
    public Double getProduct_h() {
        return product_h;
    }

    /**
     * @param product_h the product_h to set
     */
    public void setProduct_h(Double product_h) {
        this.product_h = product_h;
    }

    /**
     * @return the product_w
     */
    public Double getProduct_w() {
        return product_w;
    }

    /**
     * @param product_w the product_w to set
     */
    public void setProduct_w(Double product_w) {
        this.product_w = product_w;
    }

    /**
     * @return the product_c
     */
    public Double getProduct_c() {
        return product_c;
    }

    /**
     * @param product_c the product_c to set
     */
    public void setProduct_c(Double product_c) {
        this.product_c = product_c;
    }

    /**
     * @return the product_value
     */
    public Double getProduct_value() {
        return product_value;
    }

    /**
     * @param product_value the product_value to set
     */
    public void setProduct_value(Double product_value) {
        this.product_value = product_value;
    }

    /**
     * @return the product_description
     */
    public String getProduct_description() {
        return product_description;
    }

    /**
     * @param product_description the product_description to set
     */
    public void setProduct_description(String product_description) {
        this.product_description = product_description;
    }

    /**
     * @return the route_image
     */
    public String getRoute_image() {
        return route_image;
    }

    /**
     * @param route_image the route_image to set
     */
    public void setRoute_image(String route_image) {
        this.route_image = route_image;
    }
    private int product_id;
    private String product_name;
    private String product_type;
    private int product_dest;
    private String product_col;
    private Double product_h;
    private Double product_w;
    private Double product_c;
    private Double product_value;
    private String product_description;
    private String route_image;   
}
