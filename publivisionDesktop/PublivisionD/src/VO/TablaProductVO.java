/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import DAO.ProductoDAO;
import Render.Render;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rzunigas
 */
public class TablaProductVO {
    
    public void visualizar_ProductoVO(JTable tabla){
        
        tabla.setDefaultRenderer(Object.class, new Render());
        DefaultTableModel dt = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };    
        dt.addColumn("Id");
        dt.addColumn("Nombre");
        dt.addColumn("Descripcion");
        dt.addColumn("Cantidad");
        dt.addColumn("Valor");
        
        ProductoDAO dao = new ProductoDAO();
        ProductoVO vo = new ProductoVO();
        ArrayList<ProductoVO> list = dao.Listar_ProductoVO();

        if(list.size() > 0){
            for(int i=0; i<list.size(); i++){
                Object fila[] = new Object[6];
                vo = list.get(i);
                fila[0] = vo.getProduct_id();
                fila[1] = vo.getProduct_name();
                fila[2] = vo.getProduct_description();
                fila[3] = vo.getProduct_c();
                fila[4] = vo.getProduct_value();
                dt.addRow(fila);
            }
            tabla.setModel(dt);
            tabla.setRowHeight(20);
        }
    }
}
