/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import DAO.ClienteDAO;
import Render.Render;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rzunigas
 */
public class TablaClienteVO {
    
    public void visualizar_ClienteVO(JTable tabla){
        
        tabla.setDefaultRenderer(Object.class, new Render());
        DefaultTableModel dt = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };    
        dt.addColumn("Rut");
        dt.addColumn("Nombre");
        dt.addColumn("Apellidos");
        dt.addColumn("E-mail");
        dt.addColumn("Calle");
        dt.addColumn("Número");
        dt.addColumn("Ciudad");
        dt.addColumn("Región");
        dt.addColumn("País");
        
        ClienteDAO dao = new ClienteDAO();
        ClienteVO vo = new ClienteVO();
        ArrayList<ClienteVO> list = dao.Listar_ClienteVO();

        if(list.size() > 0){
            for(int i=0; i<list.size(); i++){
                Object fila[] = new Object[9];
                vo = list.get(i);
                fila[0] = vo.getCliente_rut();
                fila[1] = vo.getCliente_nombre();
                fila[2] = vo.getCliente_ape();
                fila[3] = vo.getCliente_correo();
                fila[4] = vo.getCliente_calle();
                fila[5] = vo.getCliente_num();
                fila[6] = vo.getCliente_ciudad();
                fila[7] = vo.getCliente_region();
                fila[8] = vo.getCliente_pais();
                dt.addRow(fila);
            }
            tabla.setModel(dt);
            tabla.setRowHeight(20);
        }
    }
}
