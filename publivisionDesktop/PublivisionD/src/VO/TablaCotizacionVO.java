/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import DAO.CotizacionDAO;
import Render.Render;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rzunigas
 */
public class TablaCotizacionVO {
    
    public void visualizar_CotizacionVO(JTable tabla){
        
        tabla.setDefaultRenderer(Object.class, new Render());
        DefaultTableModel dt = new DefaultTableModel(){
            public boolean isCellEditable(int row, int column){
                return false;
            }
        };    
        dt.addColumn("Id");
        dt.addColumn("Rut cliente");
        dt.addColumn("Nombre");
        dt.addColumn("Apellidos");
        dt.addColumn("Correo");
        dt.addColumn("Monto neto");
        dt.addColumn("Monto iva");
        dt.addColumn("Monto total");
        dt.addColumn("Fecha Creacion");
        
        CotizacionDAO dao = new CotizacionDAO();
        CotizacionVO vo = new CotizacionVO();
        ArrayList<CotizacionVO> list = dao.Listar_CotizacionVO();

        if(list.size() > 0){
            for(int i=0; i<list.size(); i++){
                Object fila[] = new Object[9];
                vo = list.get(i);
                fila[0] = vo.getCot_id();
                fila[1] = vo.getCot_rcliente();
                fila[2] = vo.getCot_ncliente();
                fila[3] = vo.getCot_acliente();
                fila[4] = vo.getCorreo();
                fila[5] = vo.getCot_neto();
                fila[6] = vo.getCot_iva();
                fila[7] = vo.getCot_total();
                fila[8] = vo.getCot_fecha();
                dt.addRow(fila);
            }
            tabla.setModel(dt);
            tabla.setRowHeight(20);
        }
    }
}
