/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import VO.CotizacionVO;
import conection.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import menu.Cotizacion;

/**
 *
 * @author rzunigas
 */
public class CotizacionDAO {
    
    public ArrayList<CotizacionVO> Listar_CotizacionVO(){
        ArrayList<CotizacionVO> list = new ArrayList<CotizacionVO>();
        Conexion cn = new Conexion();
        String sql = "SELECT cotizacion.ID_COTIZACION, cotizacion.MONTO_IVA, "
                    + "cotizacion.MONTO_NETO, cotizacion.MONTO_TOTAL, "
                    + "cliente.ID_CLIENTE, cliente.NOMBRE_RAZON, cliente.APELLIDOS, cotizacion.FECHA_CREACION, cliente.CORREO " 
                    + "FROM cotizacion LEFT JOIN cliente ON cotizacion.ID_CLIENTE = cliente.ID_CLIENTE;";
        ResultSet rs = null;
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                CotizacionVO vo = new CotizacionVO();
                vo.setCot_id(rs.getInt(1));
                vo.setCot_iva(rs.getDouble(2));
                vo.setCot_neto(rs.getDouble(3));
                vo.setCot_total(rs.getDouble(4));
                vo.setCot_rcliente(rs.getString(5));
                vo.setCot_ncliente(rs.getString(6));
                vo.setCot_acliente(rs.getString(7));
                vo.setCot_fecha(rs.getDate(8).toString());
                vo.setCorreo(rs.getString(9));
                list.add(vo);
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                rs.close();
                cn.desconectar();
            }catch(SQLException ex){
                System.out.print(ex.getMessage());
            }
        }
        return list;
    }
    public void Eliminar_CotizacionVO(CotizacionVO vo){
        Conexion cn = new Conexion();
        String sql = "DELETE FROM COTIZACION WHERE ID_COTIZACION = ?;";
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql);
            ps.setInt(1, vo.getCot_id());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
    }
    public void Eliminar_DetalleCotizacionVO(CotizacionVO vo){
        Conexion cn = new Conexion();
        String sql = "DELETE FROM `detalle_cotizacion` WHERE `detalle_cotizacion`.`ID_COTIZACION` = ?;";
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql);
            ps.setInt(1, vo.getCot_id());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
    }
}
