/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import VO.ProductoVO;
import conection.Conexion;
import crud.ProductoC;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rzunigas
 */
public class ProductoDAO {
    
    public ProductoDAO(){
    }
    
    public ArrayList<ProductoVO> Listar_ProductoVO(){
        ArrayList<ProductoVO> list = new ArrayList<ProductoVO>();
        Conexion cn = new Conexion();
        String sql = "SELECT ID_PRODUCTO, "
                     + "NOMBRE, "
                     + "DESCRIPCION, "
                     + "STOCK, "
                     + "COSTO_UNITARIO "
                     + "FROM PRODUCTO";
        ResultSet rs = null;
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                ProductoVO vo = new ProductoVO();
                vo.setProduct_id(rs.getInt(1));
                vo.setProduct_name(rs.getString(2));
                vo.setProduct_description(rs.getString(3));
                vo.setProduct_c(rs.getDouble(4));
                vo.setProduct_value(rs.getDouble(5));
                list.add(vo);
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                rs.close();
                cn.desconectar();
            }catch(SQLException ex){
                System.out.print(ex.getMessage());
            }
        }
        return list;
    }


/*Metodo agregar*/
    public void Agregar_ProductoVO(ProductoVO vo){
        Conexion cn = new Conexion();
        String sql = "INSERT INTO PRODUCTO "
                + "(NOMBRE, "
                + "DESCRIPCION, "
                + "STOCK, "
                + "RETENIDO, "
                + "TIPO_PRODUCTO, "
                + "FLAG_DESTACADO,"
                + "ID_IMAGEN, "
                + "ANCHO, "
                + "ALTO, "
                + "COLOR,"
                + "COSTO_UNITARIO) "
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?);";
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql);
            ps.setString(1, vo.getProduct_name());
            ps.setString(2, vo.getProduct_description());
            ps.setDouble(3, vo.getProduct_c());
            ps.setDouble(4, 0);
            ps.setString(5, vo.getProduct_type());
            ps.setInt(6, vo.getProduct_dest());
            ps.setString(7, vo.getRoute_image());
            ps.setDouble(8, vo.getProduct_w());
            ps.setDouble(9, vo.getProduct_h());
            ps.setString(10, vo.getProduct_col());
            ps.setDouble(11, vo.getProduct_value());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
    }


/*Metodo Modificar*/
    public void Modificar_ProductoVO(ProductoVO vo){
        Conexion cn = new Conexion();
        String sql = "UPDATE PRODUCTO SET " 
                + "NOMBRE=?, "
                + "DESCRIPCION=?, "
                + "STOCK=?, "
                + "TIPO_PRODUCTO=?, "
                + "FLAG_DESTACADO=?, "
                + "ID_IMAGEN=?, "
                + "ANCHO=?, "
                + "ALTO=?, "
                + "COLOR=?, "
                + "COSTO_UNITARIO=? "
                + "WHERE ID_PRODUCTO=?";
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql);
            ps.setString(1, vo.getProduct_name());
            ps.setString(2, vo.getProduct_description());
            ps.setDouble(3, vo.getProduct_c());
            ps.setString(4, vo.getProduct_type());
            ps.setInt(5, vo.getProduct_dest());
            ps.setString(6, vo.getRoute_image());
            ps.setDouble(7, vo.getProduct_w());
            ps.setDouble(8, vo.getProduct_h());
            ps.setString(9, vo.getProduct_col());
            ps.setDouble(10, vo.getProduct_value());
            ps.setInt(11, vo.getProduct_id());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
    }


/*Metodo Eliminar*/
    public void Eliminar_ProductoVO(ProductoVO vo){
        Conexion cn = new Conexion();
        String sql = "DELETE FROM PRODUCTO WHERE ID_PRODUCTO = ?;";
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql);
            ps.setInt(1, vo.getProduct_id());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
    }
    
    public void Consultar_ProductoVO(String id){
        ArrayList<ProductoVO> list = new ArrayList<ProductoVO>();
        Conexion cn = new Conexion();
        String sql = "SELECT "
                + "NOMBRE, "
                + "DESCRIPCION, "
                + "STOCK, "
                + "TIPO_PRODUCTO, "
                + "FLAG_DESTACADO,"
                + "ID_IMAGEN, "
                + "ANCHO, "
                + "ALTO, "
                + "COLOR,"
                + "COSTO_UNITARIO "
                + "FROM PRODUCTO WHERE ID_PRODUCTO=?;";
        ResultSet rs = null;
        PreparedStatement ps = null;
        ProductoVO vo = new ProductoVO();
        ProductoC pro = new ProductoC();
        try{
            ps = cn.getConexion().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(id));
            rs = ps.executeQuery();
            while(rs.next()){
                vo.setProduct_name(rs.getString(1));
                vo.setProduct_description(rs.getString(2));
                vo.setProduct_c(rs.getDouble(3));
                vo.setProduct_type(rs.getString(4));
                vo.setProduct_dest(rs.getInt(5));
                vo.setRoute_image(rs.getString(6));
                vo.setProduct_w(rs.getDouble(7));
                vo.setProduct_h(rs.getDouble(8));
                vo.setProduct_col(rs.getString(9));
                vo.setProduct_value(rs.getDouble(10));
                vo.setProduct_id(Integer.parseInt(id));
                list.add(vo);
            }
            pro.cargar(vo);
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(SQLException ex){
                System.out.print(ex.getMessage());
            }
        }
        pro.setVisible(true);
    }
    
}
