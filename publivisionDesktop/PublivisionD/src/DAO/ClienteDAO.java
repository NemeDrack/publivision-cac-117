/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import VO.ClienteVO;
import conection.Conexion;
import crud.ClienteC;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rzunigas
 */
public class ClienteDAO {
    
    public ArrayList<ClienteVO> Listar_ClienteVO(){
        ArrayList<ClienteVO> list = new ArrayList<ClienteVO>();
        Conexion cn = new Conexion();
        String sql = "SELECT a.ID_CLIENTE, a.NOMBRE_RAZON, a.APELLIDOS, a.CORREO, "
                    + "b.PAIS, b.REGION, b.CIUDAD, b.NOMBRE_CALLE, b.NUMERO_CALLE "
                    + "FROM cliente AS a LEFT JOIN direccion AS b ON a.ID_DIRECCION = b.ID_DIRECCION;";
        ResultSet rs = null;
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                ClienteVO vo = new ClienteVO();
                vo.setCliente_rut(rs.getString(1));
                vo.setCliente_nombre(rs.getString(2));
                vo.setCliente_ape(rs.getString(3));
                vo.setCliente_correo(rs.getString(4));
                vo.setCliente_pais(rs.getString(5));
                vo.setCliente_region(rs.getString(6));
                vo.setCliente_ciudad(rs.getString(7));
                vo.setCliente_calle(rs.getString(8));
                vo.setCliente_num(rs.getString(9));
                list.add(vo);
            }
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                rs.close();
                cn.desconectar();
            }catch(SQLException ex){
                System.out.print(ex.getMessage());
            }
        }
        return list;
    }
    public void Agregar_ClienteVO(ClienteVO vo){
        Conexion cn = new Conexion();
        String sql1 = "INSERT INTO CLIENTE "
                + "(ID_CLIENTE, "
                + "NOMBRE_RAZON, "
                + "APELLIDOS, "
                + "CORREO "
                + "VALUES(?,?,?,?);";
        String sql2 = "INSERT INTO DIRECCION "
                + "(NOMBRE_CALLE, "
                + "NUMERO_CALLE, "
                + "PAIS, "
                + "REGION,"
                + "CIUDAD "
                + "VALUES(?,?,?,?,?);";
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql1);
            ps.setString(1, vo.getCliente_rut());
            ps.setString(2, vo.getCliente_nombre());
            ps.setString(3, vo.getCliente_ape());
            ps.setString(4, vo.getCliente_correo());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
        try{
            ps = cn.getConexion().prepareStatement(sql2);
            ps.setString(1, vo.getCliente_calle());
            ps.setString(2, vo.getCliente_num());
            ps.setString(3, vo.getCliente_pais());
            ps.setString(4, vo.getCliente_region());
            ps.setString(5, vo.getCliente_ciudad());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
    }
    public void Modificar_ClienteVO(ClienteVO vo){
        Conexion cn = new Conexion();
        String sql1 = "UPDATE CLIENTE SET " 
                + "NOMBRE_RAZON=?, "
                + "APELLIDOS=?, "
                + "CORREO=? "
                + "WHERE ID_CLIENTE=?";
        String sql2 = "UPDATE DIRECCION SET " 
                + "NOMBRE_CALLE=?, "
                + "NUMERO_CALLE=?, "
                + "PAIS=?, "
                + "REGION=?, "
                + "CIUDAD=? "
                + "WHERE ID_DIRECCION=?";
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql1);
            ps.setString(1, vo.getCliente_nombre());
            ps.setString(2, vo.getCliente_ape());
            ps.setString(3, vo.getCliente_correo());
            ps.setString(4, vo.getCliente_rut());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
        try{
            ps = cn.getConexion().prepareStatement(sql2);
            ps.setString(1, vo.getCliente_calle());
            ps.setString(2, vo.getCliente_num());
            ps.setString(3, vo.getCliente_pais());
            ps.setString(4, vo.getCliente_region());
            ps.setString(5, vo.getCliente_ciudad());
            ps.setInt(6, vo.getClient_idDir());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
    }
        public void Eliminar_ClienteVO(ClienteVO vo){
        Conexion cn = new Conexion();
        String sql = "DELETE FROM CLIENTE WHERE ID_CLIENTE = ?;";
        PreparedStatement ps = null;
        try{
            ps = cn.getConexion().prepareStatement(sql);
            ps.setString(1, vo.getCliente_rut());
            ps.executeUpdate();
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(Exception ex){}
        }
    }
    
    public void Consultar_ClienteVO(String id){
        ArrayList<ClienteVO> list = new ArrayList<ClienteVO>();
        Conexion cn = new Conexion();
        String sql = "SELECT a.ID_CLIENTE, a.NOMBRE_RAZON, a.APELLIDOS, a.CORREO, "
                    + "b.PAIS, b.REGION, b.CIUDAD, b.NOMBRE_CALLE, b.NUMERO_CALLE, b.ID_DIRECCION "
                    + "FROM cliente AS a LEFT JOIN direccion AS b ON a.ID_DIRECCION = b.ID_DIRECCION "
                    + "WHERE a.ID_CLIENTE = ?";
        ResultSet rs = null;
        PreparedStatement ps = null;
        ClienteVO vo = new ClienteVO();
        ClienteC pro = new ClienteC();
        try{
            ps = cn.getConexion().prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(id));
            rs = ps.executeQuery();
            while(rs.next()){
                vo.setCliente_rut(rs.getString(1));
                vo.setCliente_nombre(rs.getString(2));
                vo.setCliente_ape(rs.getString(3));
                vo.setCliente_correo(rs.getString(4));
                vo.setCliente_pais(rs.getString(5));
                vo.setCliente_region(rs.getString(6));
                vo.setCliente_ciudad(rs.getString(7));
                vo.setCliente_calle(rs.getString(8));
                vo.setCliente_num(rs.getString(9));
                vo.setClient_idDir(rs.getInt(10));
                list.add(vo);
            }
            pro.cargar(vo);
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try{
                ps.close();
                cn.desconectar();
            }catch(SQLException ex){
                System.out.print(ex.getMessage());
            }
        }
        pro.setVisible(true);
    }
    
}
