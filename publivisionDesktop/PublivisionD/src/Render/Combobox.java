/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Render;

import javax.swing.JComboBox;

/**
 *
 * @author rzunigas
 */
public class Combobox {
    
    public static void Region(JComboBox cboreg){
        cboreg.addItem("I de Tarapacá");
        cboreg.addItem("II de Antofagasta");
        cboreg.addItem("III de Atacama");
        cboreg.addItem("IV de Coquimbo");
        cboreg.addItem("V de Valparaíso");
        cboreg.addItem("VI del Libertador General Bernardo O'Higgins");
        cboreg.addItem("VII del Maule");
        cboreg.addItem("VIII de Concepción");
        cboreg.addItem("IX de la Araucanía");
        cboreg.addItem("X de Los Lagos");
        cboreg.addItem("XI de Aysén del General Carlos Ibañez del Campo");
        cboreg.addItem("XII de Magallanes y de la Antártica Chilena");
        cboreg.addItem("Metropolitana de Santiago");
        cboreg.addItem("XIV de Los Ríos");
        cboreg.addItem("XV de Arica y Parinacota");
        cboreg.addItem("XVI del Ñuble"); 
    }
    
    public static void Ciudad(JComboBox cboreg, JComboBox cbociu){
        cbociu.removeAllItems();
        switch(cboreg.getSelectedItem().toString()){
            case "I de Tarapacá":
                cbociu.addItem("Iquique");
                cbociu.addItem("Alto Hospicio");
                cbociu.addItem("La Tirana");
            case "II de Antofagasta":
                cbociu.addItem("Antofagasta");
                cbociu.addItem("Calama");
                cbociu.addItem("San Pedro de Atacama");
            case "III de Atacama":
                cbociu.addItem("Copiapó");
                cbociu.addItem("Vallenar");
                cbociu.addItem("Caldera");
            case "IV de Coquimbo":
                cbociu.addItem("La Serene");
                cbociu.addItem("Ovalle");
                cbociu.addItem("Coquimbo");
            case "V de Valparaíso":
                cbociu.addItem("Valparaíso");
                cbociu.addItem("Viña del Mar");
                cbociu.addItem("Concón");
            case "VI del Libertador General Bernardo O'Higgins":
                cbociu.addItem("Rancagua");
                cbociu.addItem("San Fernandp");
                cbociu.addItem("Rengo");
            case "VII del Maule":
                cbociu.addItem("Talca");
                cbociu.addItem("Curicó");
                cbociu.addItem("Linares");
            case "VIII de Concepción":
                cbociu.addItem("Concepción");
                cbociu.addItem("Talcahuano");
                cbociu.addItem("San Pedro de la Paz");
            case "IX de la Araucanía":
                cbociu.addItem("Temuco");
                cbociu.addItem("Villarica");
                cbociu.addItem("Pucón");
            case "X de Los Lagos":
                cbociu.addItem("Puerto Montt");
                cbociu.addItem("Osorno");
                cbociu.addItem("Castro");
            case "XI de Aysén del General Carlos Ibañez del Campo":
                cbociu.addItem("Coyhaique");
                cbociu.addItem("Aysén");
                cbociu.addItem("Cochrane");
            case "XII de Magallanes y de la Antártica Chilena":
                cbociu.addItem("Punta Arenas");
                cbociu.addItem("Porvenir");
                cbociu.addItem("Puerto Natales");
            case "Metropolitana de Santiago":
                cbociu.addItem("Santiago");
                cbociu.addItem("San Bernardo");
                cbociu.addItem("Melipilla");
            case "XIV de Los Ríos":
                cbociu.addItem("Valdivia");
                cbociu.addItem("Corral");
                cbociu.addItem("Panguipulli");
            case "XV de Arica y Parinacota":
                cbociu.addItem("Arica");
                cbociu.addItem("Putre");
                cbociu.addItem("Parinacota");
            case "XVI del Ñuble":
                cbociu.addItem("Chillán");
                cbociu.addItem("Yungay");
                cbociu.addItem("Pinto");
                
        }
    }
}
