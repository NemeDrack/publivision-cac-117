/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author rzunigas
 */
public class Conexion {
    private static String user ="root";
    private static String pass ="";
    private static String server ="jdbc:mysql://localhost:3306/publivisionbd";
    
    public static Connection getConexion(){
        Connection cn=null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            cn = DriverManager.getConnection(server,user,pass);
        }
        catch(ClassNotFoundException | SQLException e){
            System.out.print(e.getMessage());
        }
        return cn;
    }
    public static ResultSet getTabla(String Consulta){
        Connection cn = getConexion();
        Statement st;
        ResultSet datos = null;
        try{
            st=cn.createStatement();
            datos=st.executeQuery(Consulta);
        }
        catch(SQLException e){
            System.out.print(e.toString());
        }
        return datos;
    }
    
    public void desconectar(){
        Connection cn = null;
    }
    
}
